from subprocess import Popen, PIPE
import subprocess
from ftfy import fix_text
import lxml
from lxml import etree
import urllib.request

with open('C:\\Users\\Kevin\\Desktop\\WGT\\chronosphere\\utils\\ytlist.txt') as f:
    content = f.readlines()
content = [x.strip() for x in content]

for url in content:
    youtube = etree.HTML(urllib.request.urlopen(url).read())
    video_title = youtube.xpath("//span[@id='eow-title']/@title")
    filename = fix_text(''.join(video_title))
    youtube_cmd = 'youtube-dl -o "' + filename + '" "' + url +'"'
    subprocess.call(youtube_cmd, shell=True)