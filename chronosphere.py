from subprocess import Popen, PIPE
import subprocess
import argparse
import io
import os
import json
import pickle
from ankipan import Ankipan
from google.cloud import videointelligence, vision
from hostcurry import youtube

def conduct(ankipan, url, host, name):
    switcher = {
        "youtube": youtube
    }
    func = switcher.get(host, lambda: "nothing")
    return func(ankipan, url, name)

def manipulator(filename, ext):
    extract_cmd = 'ffmpeg -i ' + filename + '.' + ext + ' -ab 160k -ac 1 -ar 44100 -vn ' + filename + '.wav'
    subprocess.call(extract_cmd, shell=True)

    convert_cmd = 'ffmpeg -i ' + filename + '.' + ext + ' -f mp4 -vcodec libx264 -s 1920x1080 -acodec aac ' + filename + '.mp4'
    subprocess.call(convert_cmd, shell=True)
        
def transcribe_gcs(ankipan, gcs_uri, lang):
    """Asynchronously transcribes the audio file specified by the gcs_uri."""
    from google.cloud import speech
    from google.cloud.speech import enums
    from google.cloud.speech import types
    client = speech.SpeechClient()

    audio = types.RecognitionAudio(uri=gcs_uri)
    config = types.RecognitionConfig(
        sample_rate_hertz=44100,
        language_code=lang,
        enable_word_time_offsets=True)

    operation = client.long_running_recognize(config, audio)

    print('Waiting for operation to complete...')
    response = operation.result(timeout=1000)

    # Each result is for a consecutive portion of the audio. Iterate through
    # them to get the transcripts for the entire audio file.
    ankipan.gulp('"audios": [')
    for i, result in enumerate(response.results):
        if i != 0:
            ankipan.eat(',')
        # The first alternative is the most likely one for this portion.
        print('Transcript: {}'.format(result.alternatives[0].transcript))
        ankipan.gulp('{ "transcript": "' + result.alternatives[0].transcript + '", ')
        print('Confidence: {}'.format(result.alternatives[0].confidence))
        ankipan.gulp('"confidence": ' + str(result.alternatives[0].confidence) + ', ')
        ankipan.gulp('"words": [')

        for k, word_info in enumerate(result.alternatives[0].words):
            if k != 0:
                ankipan.eat(',')
            word = word_info.word
            start_time = word_info.start_time
            end_time = word_info.end_time
            print('Word: {}, start_time: {}, end_time: {}'.format(
                word,
                start_time.seconds + start_time.nanos * 1e-9,
                end_time.seconds + end_time.nanos * 1e-9))
            ankipan.gulp('{ "word": "' + word + '",')
            ankipan.gulp('"start": ' + str(start_time.seconds + start_time.nanos * 1e-9) + ',')
            ankipan.gulp('"end": ' + str(end_time.seconds + end_time.nanos * 1e-9) + '}')

        ankipan.gulp(']')
        ankipan.gulp('}')
    ankipan.gulp(']')


def analyze_labels_gcs(ankipan, path, name):
    """ Detects labels given a GCS path. """
    video_client = videointelligence.VideoIntelligenceServiceClient()
    features = [videointelligence.enums.Feature.LABEL_DETECTION]

    mode = videointelligence.enums.LabelDetectionMode.SHOT_AND_FRAME_MODE
    config = videointelligence.types.LabelDetectionConfig(label_detection_mode=mode)
    context = videointelligence.types.VideoContext(label_detection_config=config)

    operation = video_client.annotate_video(path, features=features, video_context=context)
    print('\nProcessing video for label annotations:')

    result = operation.result(timeout=1000)
    print('\nFinished processing.')

    # Process video/segment level label annotations
    segment_labels = result.annotation_results[0].segment_label_annotations

    ankipan.gulp('"videos": [')
    for i, segment_label in enumerate(segment_labels):
        if i != 0:
            ankipan.eat(',')

        print('Video label description: {}'.format(segment_label.entity.description))
        #ankipan.eat('Video label description: {}'.format(segment_label.entity.description))
        ankipan.eat('{"description": "' + segment_label.entity.description + '",')

        ankipan.gulp('"categories": [')
        for j, category_entity in enumerate(segment_label.category_entities):
            if j != 0:
                ankipan.eat(',')

            print('\tLabel category description: {}'.format(category_entity.description))
            #ankipan.eat('\tLabel category description: {}'.format(category_entity.description))
            ankipan.eat('{"category": "' + category_entity.description + '"}')
        ankipan.gulp('],')

        ankipan.gulp('"data": [')
        for i, segment in enumerate(segment_label.segments):
            if i != 0:
                ankipan.eat(',')

            start_time = (segment.segment.start_time_offset.seconds + segment.segment.start_time_offset.nanos / 1e9)
            end_time = (segment.segment.end_time_offset.seconds + segment.segment.end_time_offset.nanos / 1e9)
            positions = '{}s to {}s'.format(start_time, end_time)
            confidence = segment.confidence
            print('\tSegment {}: {}'.format(i, positions))
            #ankipan.eat('\tSegment {}: {}'.format(i, positions))
            ankipan.eat('{"segment": { "index": ' + str(i) + ', "start": ' + str(start_time) + ', "end": ' + str(end_time) + '},')

            print('\tConfidence: {}'.format(confidence))
            #ankipan.eat('\tConfidence: {}'.format(confidence))
            ankipan.gulp('"confidence": ' + str(confidence) + '}')

        ankipan.gulp(']}')

        print('\n')
        #ankipan.newline()

    ankipan.gulp('],')

    # Process shot level label annotations
    shot_labels = result.annotation_results[0].shot_label_annotations
    ankipan.gulp('"shots": [')
    for i, shot_label in enumerate(shot_labels):
        if i != 0:
            ankipan.eat(',')

        print('Shot label description: {}'.format(shot_label.entity.description))
        #ankipan.eat('Shot label description: {}'.format(shot_label.entity.description))
        ankipan.eat('{"description": "' + shot_label.entity.description + '",')

        ankipan.gulp('"categories": [')

        count_cat = 0
        for category_entity in shot_label.category_entities:
            if count_cat != 0:
                ankipan.eat(',')

            print('\tLabel category description: {}'.format(category_entity.description))
            #ankipan.eat('\tLabel category description: {}'.format(category_entity.description))
            ankipan.eat('{"category": "' + category_entity.description + '"}')
            count_cat = count_cat + 1

        ankipan.gulp('],')

        ankipan.gulp('"data": [')
        for i, shot in enumerate(shot_label.segments):
            if i != 0:
                ankipan.eat(',')

            start_time = (shot.segment.start_time_offset.seconds + shot.segment.start_time_offset.nanos / 1e9)
            end_time = (shot.segment.end_time_offset.seconds + shot.segment.end_time_offset.nanos / 1e9)
            positions = '{}s to {}s'.format(start_time, end_time)
            confidence = shot.confidence
            print('\tSegment {}: {}'.format(i, positions))
            #ankipan.eat('\tSegment {}: {}'.format(i, positions))
            ankipan.eat('{"segment": { "index": ' + str(i) + ', "start": ' + str(start_time) + ', "end": ' + str(end_time) + '},')

            print('\tConfidence: {}'.format(confidence))
            #ankipan.eat('\tConfidence: {}'.format(confidence))
            ankipan.gulp('"confidence": ' + str(confidence) + '}')
        ankipan.gulp(']')

        print('\n')
        ankipan.gulp('}')
        #ankipan.newline()

    ankipan.gulp('],')

    # Process frame level label annotations
    frame_labels = result.annotation_results[0].frame_label_annotations
    ankipan.gulp('"frames": [')
    for i, frame_label in enumerate(frame_labels):
        if i != 0:
            ankipan.eat(',')

        print('Frame label description: {}'.format(frame_label.entity.description))
        #ankipan.eat('Frame label description: {}'.format(frame_label.entity.description))
        ankipan.eat('{"description": "' + frame_label.entity.description + '",')

        ankipan.gulp('"categories": [')
        count_cat = 0
        for category_entity in frame_label.category_entities:
            if count_cat != 0:
                ankipan.eat(',')

            print('\tLabel category description: {}'.format(category_entity.description))
            #ankipan.eat('\tLabel category description: {}'.format(category_entity.description))
            ankipan.eat('{"category": "' + category_entity.description + '"}')

            count_cat = count_cat + 1
            
        ankipan.gulp('],')

        # Each frame_label_annotation has many frames,
        # here we print information only about the first frame.
        frame = frame_label.frames[0]
        time_offset = (frame.time_offset.seconds + frame.time_offset.nanos / 1e9)
        print('\tFirst frame time offset: {}s'.format(time_offset))
        #ankipan.eat('\tFirst frame time offset: {}s'.format(time_offset))
        ankipan.eat('"offset": ' + str(time_offset) + ',');

        print('\tFirst frame confidence: {}'.format(frame.confidence))
        #ankipan.eat('\tFirst frame confidence: {}'.format(frame.confidence))
        ankipan.eat('"confidence": ' + str(frame.confidence) + '}')
        print('\n')
        #ankipan.newline()

    ankipan.gulp(']')
    
    #ankipan.newline()

def extract_image_from_video(video_input, name_output, time_stamp):
    ret = "Error"
    try:
        ret = os.system("ffmpeg -i " + video_input + " -ss " + time_stamp + " -frames:v 1 " + name_output)    
        # if all goes well FFMPEG will return 0
        return ret
    except ValueError:
        return("Oops! error...")

def detect_face(face_file, max_results=4):
    from google.cloud.vision import types
    # find a face and return its coordinates
    client = vision.ImageAnnotatorClient()
    content = face_file.read()
    image = types.Image(content=content)

    # return coords of face
    return client.face_detection(image=image).face_annotations

def highlight_faces(image, faces):
    from PIL import Image, ImageDraw
    # Draws a polygon around the faces, then saves to output_filename.
    faces_boxes = []
    im = Image.open(image)
    draw = ImageDraw.Draw(im)

    for face in faces:
        box = [(vertex.x, vertex.y)
               for vertex in face.bounding_poly.vertices]
        draw.line(box + [box[0]], width=5, fill='#00ff00')
        faces_boxes.append([box[0][0], box[0][1], box[1][0] - box[0][0], box[3][1] - box[0][1]])
    return (faces_boxes)

def crop_image(input_image, output_image, start_x, start_y, width, height):
    from PIL import Image, ImageDraw
    import numpy as np
    """Pass input name image, output name image, x coordinate to start croping, y coordinate to start croping, width to crop, height to crop """
    input_img = Image.open(input_image)
    # give the image some buffer space
    start_with_buffer_x = int(start_x - np.ceil(width/2))
    start_with_buffer_y = int(start_y - np.ceil(height/2))
    width_with_buffer = int(start_x + width  + np.ceil(width/2))
    height_with_buffer = int(start_y + height  + np.ceil(height/2))

    box = (start_with_buffer_x, start_with_buffer_y, width_with_buffer, height_with_buffer)
    output_img = input_img.crop(box)
    output_img.save(output_image +".png")
    return (output_image +".png")

def annotate(path):
    from google.cloud.vision import types
    """Returns web annotations given the path to an image."""
    client = vision.ImageAnnotatorClient()

    if path.startswith('http') or path.startswith('gs:'):
        image = types.Image()
        image.source.image_uri = path
    else:
        with io.open(path, 'rb') as image_file:
            content = image_file.read()

        image = types.Image(content=content)

    web_detection = client.web_detection(image=image).web_detection

    return web_detection