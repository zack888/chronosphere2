import json
from collections import OrderedDict

def awsCredLoader(path):
    with open(path) as data_file:
        data = OrderedDict(json.load(data_file))
    return data["access"], data["seckey"]