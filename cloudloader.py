from google.cloud import storage
import io

def _get_storage_client():
    return storage.Client(project='chronosphere-194101')

def upload(filename):
    client = _get_storage_client()
    bucket = client.bucket('ctestvids')
    blob = bucket.blob(filename)
    blob.upload_from_filename(filename)
    
def delete(filename):
    client = _get_storage_client()
    bucket = client.bucket('ctestvids')
    blob = bucket.blob(filename)
    blob.delete()