from googletrans import Translator

def whatLanguage(label):
    translator = Translator()
    lang = translator.detect(label)

    return {
        'en': 'en-US',
        'zh-cn': 'cmn-Hans-CN',
        'zh-tw': 'cmn-Hant-TW',
        'ja': 'ja-JP',
        'ko': 'ko-KR'
    }.get(lang.lang.lower(), 'en-US')