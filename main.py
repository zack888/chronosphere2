from chronosphere import conduct, manipulator, analyze_labels_gcs, transcribe_gcs
from cleaner import clean
from ankipan import Ankipan
from sanitizer import sanitize
from cloudloader import upload, delete
import os
import boto3
import json
import datetime
import time
import decimal
from nonce import nonce
from smarteyes import getyoutube
from polyglotter import whatLanguage
import sys
from ftfy import fix_text, fix_file
from awscreds import awsCredLoader

# Configurations
print('\nConfiguring OS Environment...')
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Chronosphere-c7c8a8da2845.json"
awsAccessKey, awsSecretKey = awsCredLoader("Chronosphere-aws.json")

# Script Settings
print('\nConfiguring Script Environment...')
aws_enable = True
manual_enable = False
conduct_enable = True
chrono_enable = True
clean_enable = True
success = True
pollTimeInSeconds = 20  # Max 20 seconds

# Cloud (Auto)
url = "" #"https://www.youtube.com/watch?v=5eo9HtQuNGg"
vidtype = "youtube"
host = "youtube"
id = ""
username = ""
timestamp = ""
filename = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S') + nonce()

# Local (Manual)
file = "videoPanteneBottle"
ext = "mov"

lang = ''

def awsStart():
    print('\nStart AWS incoming listening service')

    sqs = boto3.client('sqs', region_name='ap-southeast-1', aws_access_key_id=awsAccessKey, aws_secret_access_key=awsSecretKey)
    queue_url = "https://sqs.ap-southeast-1.amazonaws.com/874597036051/WGT_NewVideo"
    response = sqs.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=1,
        MessageAttributeNames=[
            'All'
        ],
        VisibilityTimeout=0,
        WaitTimeSeconds=pollTimeInSeconds
    )

    if 'Messages' in response:
        print('\nIncoming job, start processing...')

        message = response['Messages'][0]
        body = json.loads(message['Body'])
        subject = body['Subject']

        timestamp = subject.split('.')[0].replace('"', '')
        username = subject.split('.')[1].replace('"', '')
        url = body['Message'].replace('"', '')

        receipt_handle = message['ReceiptHandle']

        print('\nCleaning up AWS queue')
        sqs.delete_message(
            QueueUrl=queue_url,
            ReceiptHandle=receipt_handle
        )
        print('Received and deleted message: %s' % message)

        print('\nPreemptive URL %s sanitizing started...' % url)
        host, id, url = sanitize(url)
        print('For ID %s' % id)
        print('Getting from %s' % host)

        youtubename = fix_text(getyoutube(url))
        lang = whatLanguage(youtubename)

        print('This title is ' + youtubename + ' with ' + lang + ' encoding')

        print('\nUpdating AWS DB for job acceptance...')
        dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1', aws_access_key_id=awsAccessKey, aws_secret_access_key=awsSecretKey)
        table = dynamodb.Table('WGT_Video')
        response = table.update_item(
            Key={
                'username': username,
                'timestamp': decimal.Decimal(timestamp)
            },
            UpdateExpression="set stats = :s, video = :v, title = :t, hoster = :k",
            ExpressionAttributeValues={
                ':s': decimal.Decimal(10),
                ':v': filename,
                ':t': youtubename,
                ':k': 'Youtube'
            },
            ReturnValues="UPDATED_NEW"
        )

        print('\nAWS Pre-Workflow completed!')
        return True, timestamp, username, url, lang, youtubename, host, id
    else:
        print('\nNo new task!')
        return False, "", "", "", "", "", "", ""

# AWS Service
if aws_enable:
    success, timestamp, username, url, lang, title, host, id = awsStart()

if success:
    # Start executing CONDUCT Engine
    if manual_enable:
        print('\nManual Mode Enabled')
        ankipan = Ankipan(filename)
        ankipan.gulp('{')
        manipulator(file, ext)
        print('\nManipulator completed, uploading...')
        upload(file + ".mp4")
        print('\nUpload completed')

    elif conduct_enable:
        print('\nCONDUCT Mode Enabled')
        if not aws_enable:
            host, id, url = sanitize(url)
        ankipan = Ankipan(filename)
        ankipan.gulp('{')
        print('\nSanitizing completed, conducting...')
        ankipan.gulp('"url": "' + url + '",')
        ankipan.gulp('"title": "' + title + '",')
        name = host + id
        conduct(ankipan, url, host, name)
        print('\nConduct completed, uploading...')
        upload(name + ".mp4")
        upload(name + ".wav")

    # Start Chronosphere Engine
    if manual_enable:
        print('\nStart analyzing Labels in GCS')
        analyze_labels_gcs(ankipan, "gs://ctestvids/" + file + ".mp4", file)
        ankipan.eat('}')
        print('\nAnalysis completed for Labels in GCS')

    elif chrono_enable:
        print('\nPredicting possible language used in video: ' + title)
        print('\n' + lang +' detected')
        print('\nStart analyzing Labels in GCS')
        analyze_labels_gcs(ankipan, "gs://ctestvids/" + name + ".mp4", name)
        print('\nAnalysis completed for Labels in GCS')
        ankipan.eat(',')
        print('\nStart analyzing Audio in GCS')
        transcribe_gcs(ankipan, "gs://ctestvids/" + name + ".wav", lang)
        print('\nAnalysis completed for Audio in GCS')
        ankipan.eat('}')

    # Start cleanup
    if manual_enable:
        print('\nStart cleaning in Local')
        delete(file + ".mp4")
        delete(file + ".wav")
        print('\nStart cleaning in GCS')
        clean(file)
        print('\nCleaning completed')
        ankipan.sleep()

    elif clean_enable:
        print('\nStart cleaning in GCS')
        delete(name + ".mp4")
        delete(name + ".wav")
        print('\nStart cleaning in Local')
        clean(name)
        clean(name + "_temp")
        print('\nCleaning completed')
        ankipan.sleep()

    # Start upload to S3
    print('\nFix JSON encoding file')
    fix_file(filename + '.json')

    print('\nUploading results to S3')
    s3 = boto3.resource('s3', region_name='ap-southeast-1', aws_access_key_id=awsAccessKey, aws_secret_access_key=awsSecretKey)
    bucket = s3.Bucket('viduce')
    bucket.upload_file(filename + '.json', filename + '.json', ExtraArgs={'ACL':'public-read'})
    print('\nUpload to S3 completed')

    print('\nUpdating AWS DB for job completion...')
    dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1', aws_access_key_id=awsAccessKey, aws_secret_access_key=awsSecretKey)
    table = dynamodb.Table('WGT_Video')
    response = table.update_item(
        Key={
            'username': username,
            'timestamp': decimal.Decimal(timestamp)
        },
        UpdateExpression="set stats = :s",
        ExpressionAttributeValues={
            ':s': decimal.Decimal(100)
        },
        ReturnValues="UPDATED_NEW"
    )
    print('\nAWS Post-Workflow completed!')

    print('\nPost-upload cleanup')
    clean(filename)
    print('\nCleanup completed')
    print('\nEnd of task')
